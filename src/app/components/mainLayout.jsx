import { Button } from "@nextui-org/react";
import React from "react";

export default function MainLayout({ children }) {
  return (
    <>
      <main className="max-w-[1100px] mx-auto">
        <div className="p-4 border-b">
          <h1 className="text-center text-3xl">NEXT UI</h1>
          <div className="flex flex-col ">{children}</div>
        </div>

        <div id="label-route" className="m-6 flex gap-1">
          <Button size="sm">Avatar</Button>
          <Button size="sm">Button</Button>
          <Button size="sm">Avatar</Button>
        </div>
      </main>
    </>
  );
}
